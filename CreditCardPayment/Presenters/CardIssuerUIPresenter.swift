//
//  CardIssuerUIPresenter.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 21/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import Foundation

protocol CardIssuerUIPresenter {
    func showList(_ cardIssuers : [CardIssuer])
    func showError()
}
