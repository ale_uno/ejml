//
//  PaymentMethodUI.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 21/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import Foundation

protocol PaymentMethodsUIPresenter {
    func showList(_ creditCards : [CreditCard])
    func showError()
}
