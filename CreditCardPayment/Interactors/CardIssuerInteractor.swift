//
//  CardIssuerInteractor.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 21/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import Foundation

class CardIssuerInteractor {
    
    private var presenter: CardIssuerUIPresenter
    private var gateway : CardIssuerGateway?
    
    init(presenter: CardIssuerUIPresenter){
        self.presenter = presenter
        self.gateway = CardIssuerGateway(delegate: self)
    }
    
    func load(){
        self.gateway?.load(paymentMethodID: Environment.creditCard!.id)
    }
    
    func loadWithList(_ cardIssuers : [CardIssuer]) -> Void {

        if cardIssuers.count > 0 {
            self.presenter.showList(cardIssuers)
        } else {
            self.presenter.showError()
        }
    }
    
    func loadWithError() {
        self.presenter.showError()
    }
    
}
