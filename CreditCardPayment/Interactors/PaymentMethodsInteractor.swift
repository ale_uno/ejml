//
//  PaymentMethodsInteractor.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 19/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import UIKit

class PaymentMethodsInteractor {

    private var presenter: PaymentMethodsUIPresenter
    private var gateway : PaymentMethodGateway?
    
    init(presenter: PaymentMethodsUIPresenter){
        self.presenter = presenter
        self.gateway = PaymentMethodGateway(delegate: self)
    }
    
    func load(){
        self.gateway?.load()
    }
    
    func loadWithList(_ creditCards : [CreditCard]) -> Void {

        if creditCards.count > 0 {
            self.presenter.showList(creditCards)
        } else {
            self.presenter.showError()
        }
    }
    
    func loadWithError() {
        self.presenter.showError()
    }
    
}
