//
//  InstallmentInteractor.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 21/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import Foundation

class InstallmentInteractor {
    
    private var presenter: InstallmentUIPresenter
    private var gateway : InstallmentGateway?
    
    init(presenter: InstallmentUIPresenter){
        self.presenter = presenter
        self.gateway = InstallmentGateway(delegate: self)
    }
    
    func load(){
        self.gateway?.load(amount: String(Environment.amount),
                           paymentMethodID: Environment.creditCard!.id,
                           issuerID: Environment.cardIssuer!.id)
    }
    
    func loadWithList(_ installments : [Installment]) -> Void {
        
        if installments.count > 0 {
            self.presenter.showList(installments)
        } else {
            self.presenter.showError()
        }
    }
    
    func loadWithError() {
        self.presenter.showError()
    }
    
}

