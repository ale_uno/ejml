//
//  Environment.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 19/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import Foundation

class Environment {
    static var  amount : Double = 0.0
    static var  creditCard : CreditCard?
    static var  cardIssuer : CardIssuer?
    static var  installment : Installment?
}
