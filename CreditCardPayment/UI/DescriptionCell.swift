//
//  DescriptionCell.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 19/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import UIKit

class DescriptionCell: UITableViewCell {

    @IBOutlet weak var paymentImageView : UIImageView!
    @IBOutlet weak var paymentTypeLabel : UILabel!
    var creditCard : CreditCard?
    var cardIssuer : CardIssuer?
    var installment : Installment?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
