//
//  PaymentMethodViewController.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 19/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class PaymentMethodViewController: UIViewController {

    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var emptyLabel : UILabel!
    @IBOutlet weak var loadingView : UIView!
    var creditCards : [CreditCard] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        load()
    }
    
    private func load() {
        self.loadingView.isHidden = false
        let interactor = PaymentMethodsInteractor(presenter: self)
        interactor.load()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension PaymentMethodViewController : PaymentMethodsUIPresenter {

    func showList(_ creditCards : [CreditCard]) {
        self.creditCards = creditCards
        self.tableView.reloadData()
        self.tableView.isHidden = false
        self.loadingView.isHidden = true
    }
    
    func showError() {
        self.emptyLabel.isHidden = false
        self.loadingView.isHidden = true
    }
    
}

extension PaymentMethodViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Selección de medio de pago"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.creditCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentMethod", for: indexPath) as! DescriptionCell
        let creditCard = self.creditCards[indexPath.row]
        cell.paymentTypeLabel.text = creditCard.name
        cell.creditCard = creditCard
        
        Alamofire.request(creditCard.secureThumbnail).responseImage { response in
            if let image = response.result.value {
                cell.paymentImageView.image = image
            }
        }
        
        return cell
    }
    
}

extension PaymentMethodViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let creditCardCell = tableView.cellForRow(at: indexPath) as! DescriptionCell
        if let creditCard = creditCardCell.creditCard {
            Environment.creditCard = creditCard
            performSegue(withIdentifier: "openCardIssuerScreen", sender: self)
        }
    }
}







