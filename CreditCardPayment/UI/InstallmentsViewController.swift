//
//  InstallmentsViewController.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 21/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import UIKit

class InstallmentsViewController: UIViewController {

    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var emptyLabel : UILabel!
    @IBOutlet weak var loadingView : UIView!
    var installments : [Installment] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        load()
    }
    
    private func load() {
        self.loadingView.isHidden = false
        let interactor = InstallmentInteractor(presenter: self)
        interactor.load()
    }
    
    private func showFinalMessage() {
        
        let amountString = "Monto: " + String(Environment.amount)
        let paymentString = "\nMedio de Pago: " + Environment.creditCard!.name
        let cardIssuerString = "\nBanco: " + Environment.cardIssuer!.name
        let installmentString = "\nCantidad de Cuotas: " + Environment.installment!.recommendedMessage
        let finalMessage = amountString + paymentString + cardIssuerString + installmentString
        let uiAlert = UIAlertController(title: "Información de Pago", message: finalMessage, preferredStyle: .alert)

        uiAlert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { action in print("Aceptar")}))

        self.present(uiAlert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension InstallmentsViewController : InstallmentUIPresenter {
    
    func showList(_ installments : [Installment]) {
        print("CardIssuerViewController::showList")
        self.tableView.isHidden = false
        self.loadingView.isHidden = true
        self.installments = installments
        self.tableView.reloadData()
    }
    
    func showError() {
        print("CardIssuerViewController::showError")
        self.emptyLabel.isHidden = false
        self.loadingView.isHidden = true
    }
    
}

extension InstallmentsViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Selección de cuotas"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.installments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Installment", for: indexPath) as! DescriptionCell
        let installment = self.installments[indexPath.row]
        cell.paymentTypeLabel.text = installment.recommendedMessage
        cell.installment = installment
        return cell
    }
    
}

extension InstallmentsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Installment selected")
        let installmentCell = tableView.cellForRow(at: indexPath) as! DescriptionCell
        
        if let installment = installmentCell.installment {
            Environment.installment = installment
            self.navigationController?.popToRootViewController(animated: true)
            self.showFinalMessage()
        }
    }
}

