//
//  CardIssuerViewController.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 21/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class CardIssuerViewController: UIViewController {

    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var emptyLabel : UILabel!
    @IBOutlet weak var loadingView : UIView!
    var cardIssuers : [CardIssuer] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        load()
    }
    
    private func load() {
        self.loadingView.isHidden = false
        let interactor = CardIssuerInteractor(presenter: self)
        interactor.load()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension CardIssuerViewController : CardIssuerUIPresenter {
    
    func showList(_ cardIssuers : [CardIssuer]) {
        self.tableView.isHidden = false
        self.loadingView.isHidden = true
        self.cardIssuers = cardIssuers
        self.tableView.reloadData()
    }
    
    func showError() {
        self.emptyLabel.isHidden = false
        self.loadingView.isHidden = true
    }
    
}

extension CardIssuerViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Selección de banco"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cardIssuers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardIssuer", for: indexPath) as! DescriptionCell
        let cardIssuer = self.cardIssuers[indexPath.row]
        cell.paymentTypeLabel.text = cardIssuer.name
        cell.cardIssuer = cardIssuer
        Alamofire.request(cardIssuer.secureThumbnail).responseImage { response in
            if let image = response.result.value {
                cell.paymentImageView.image = image
            }
        }
        return cell
    }
    
}

extension CardIssuerViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cardIssuerCell = tableView.cellForRow(at: indexPath) as! DescriptionCell
        if let cardIssuer = cardIssuerCell.cardIssuer {
            Environment.cardIssuer = cardIssuer
            performSegue(withIdentifier: "openInstallmentScreen", sender: self)
        }
    }
}


