//
//  AmountViewController.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 19/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import UIKit

class AmountViewController: UIViewController {

    @IBOutlet weak var montoTextField: UITextField! {
        didSet { montoTextField.addDoneCancelToolbar(onDone: (target: self, action:#selector(doneButtonTapped))) }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.montoTextField.text = ""
    }
    
    @objc private func doneButtonTapped() {
        print("Done button tapped...")

        if let selectedAmount = self.montoTextField.text?.doubleValue {
            if selectedAmount > 0.0 {
                print("Value is OK")
                Environment.amount = selectedAmount
                self.montoTextField.resignFirstResponder()
                performSegue(withIdentifier: "openPaymentScreen", sender: self)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

