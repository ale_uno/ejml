//
//  JSONToCreditCardParser.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 21/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import Foundation

class JSONToCreditCardParser {
    
    private static let KEY_ID: String = "id"
    private static let KEY_NAME: String = "name"
    private static let KEY_THUMBNAIL: String = "thumbnail"
    private static let KEY_SECURE_THUMBNAIL: String = "secure_thumbnail"

    static func parse(_ response: [Dictionary<String, Any>]) -> [CreditCard] {
        var creditCards : [CreditCard] = []

        for jsonCreditCardItem in response {
            let creditCard = CreditCard(id: jsonCreditCardItem[KEY_ID] as! String,
                                        name: jsonCreditCardItem[KEY_NAME] as! String,
                                        thumbnail: jsonCreditCardItem[KEY_THUMBNAIL] as! String,
                                        secureThumbnail: jsonCreditCardItem[KEY_SECURE_THUMBNAIL] as! String)
            creditCards.append(creditCard)
        }

        return creditCards
    }
    
}
