//
//  JSONToCardIssuerParser.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 21/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import Foundation

class JSONToCardIssuerParser {
    
    private static let KEY_ID: String = "id"
    private static let KEY_NAME: String = "name"
    private static let KEY_THUMBNAIL: String = "thumbnail"
    private static let KEY_SECURE_THUMBNAIL: String = "secure_thumbnail"
    
    static func parse(_ response: [Dictionary<String, Any>]) -> [CardIssuer] {
        var cardIssuers : [CardIssuer] = []
        
        for jsonCardIssuerItem in response {
            let cardIssuer = CardIssuer(id: jsonCardIssuerItem[KEY_ID] as! String,
                                        name: jsonCardIssuerItem[KEY_NAME] as! String,
                                        thumbnail: jsonCardIssuerItem[KEY_THUMBNAIL] as! String,
                                        secureThumbnail: jsonCardIssuerItem[KEY_SECURE_THUMBNAIL] as! String)
            cardIssuers.append(cardIssuer)
        }
        
        return cardIssuers
    }
    
}
