//
//  JSONToInstallmentParser.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 21/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import Foundation

class JSONToInstallmentParser {
    
    private static let KEY_INSTALLMENT: String = "installments"
    private static let KEY_RECOMMENDED_MESSAGE: String = "recommended_message"
    private static let KEY_NODE: String = "payer_costs"

    static func parse(_ response: [Dictionary<String, Any>]) -> [Installment] {
        var installments : [Installment] = []
        let jsonInstallments = response[0][KEY_NODE] as! [Dictionary<String, Any>]
        for jsonInstallment in jsonInstallments {
            let installment = Installment(installments: jsonInstallment[KEY_INSTALLMENT] as! Int,
                                          recommendedMessage: jsonInstallment[KEY_RECOMMENDED_MESSAGE] as! String)
            installments.append(installment)
        }
        
        return installments
    }
    
}
