//
//  Installment.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 21/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import Foundation

class Installment {
    
    var installments : Int
    var recommendedMessage : String
    
    init(installments: Int, recommendedMessage: String) {
        self.installments = installments
        self.recommendedMessage = recommendedMessage
    }
    
}

