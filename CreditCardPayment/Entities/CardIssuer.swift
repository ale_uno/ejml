//
//  CardIssuer.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 21/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import Foundation

class CardIssuer {
    
    var id : String
    var name : String
    var thumbnail : String
    var secureThumbnail : String
    
    init(id: String, name: String, thumbnail: String, secureThumbnail: String) {
        self.id = id
        self.name = name
        self.thumbnail = thumbnail
        self.secureThumbnail = secureThumbnail
    }
    
}
