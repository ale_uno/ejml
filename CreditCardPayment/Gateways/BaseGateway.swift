//
//  BaseGateway.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 19/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import UIKit

class BaseGateway: APIListenerDelegate {

    private var requestManager : APIRequestManager?
    private let base_url = "https://api.mercadopago.com/v1/"
    private var params = "?public_key=444a9ef5-8a6b-429f-abdf-587639155d88"
    
    init() {
        self.requestManager = APIRequestManager()
    }
    
    func performGETRequest(uri: String, customParams: (Dictionary<String,String>) = [:]) {
        
        for (key,value) in customParams {
            self.params += "&" + key + "=" + value
        }
        
        self.requestManager?.performRequest(baseURL: self.base_url, uri: uri, params: self.params, method: .GET, delegate:self)
    }
    
    func cancel() {
        self.requestManager?.cancel()
    }

    func onResponse(response: [Any]) {
        preconditionFailure("This method must be overridden")
    }
    
    func onErrorResponse(statusCode: Int?, body: String?) {
        preconditionFailure("This method must be overridden")
    }
    
}

