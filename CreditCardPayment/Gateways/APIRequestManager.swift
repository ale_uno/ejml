//
//  APIRequestManager.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 19/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import Alamofire
import UIKit

enum RequestMethod {
    case GET
    case POST
}

class APIRequestManager: NSObject {

    private let CONTENT_TYPE_JSON: String = "application/json"
    private var urlRequest : Alamofire.DataRequest?

    func performRequest(baseURL : String, uri: String, params: String, method: RequestMethod, delegate: APIListenerDelegate) {

        let finalURL = baseURL + uri + params
        
        let httpMethod : HTTPMethod = (method == .GET ? .get : .post)
        
        urlRequest = Alamofire.request(finalURL, method: httpMethod)
            .validate(statusCode: 200..<300)
            .validate(contentType: [CONTENT_TYPE_JSON])
            .responseJSON(completionHandler: {
                (response) in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200:
                        if let result = response.result.value {
                            let JSON = result as! [Any]
                            delegate.onResponse(response: JSON)
                        }
                    default:
                        delegate.onErrorResponse(statusCode: status, body: response.error?.localizedDescription)
                    }
                }
            }
        )
    }
    
    func cancel() {
        self.urlRequest?.cancel()
    }
    
}
