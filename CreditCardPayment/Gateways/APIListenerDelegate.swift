//
//  APIListenerDelegate.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 19/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import Foundation

protocol APIListenerDelegate {
    func onResponse(response: [Any])
    func onErrorResponse(statusCode: Int?, body: String?)
}
