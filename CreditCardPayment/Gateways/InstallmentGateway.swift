//
//  InstallmentGateway.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 21/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import UIKit

class InstallmentGateway: BaseGateway {
   
    private let uri = "payment_methods/installments"
    private var delegate : InstallmentInteractor
    
    init(delegate: InstallmentInteractor) {
        self.delegate = delegate
        super.init()
    }
    
    func load(amount: String, paymentMethodID : String, issuerID: String) {
        let customParams = ["amount":amount, "payment_method_id":paymentMethodID, "issuer.id":issuerID]
        self.performGETRequest(uri: uri, customParams: customParams)
    }
    
    override func onResponse(response: [Any]) {
        let dictInstallments = response as![Dictionary<String,Any>]
        let installments = JSONToInstallmentParser.parse(dictInstallments)
        self.delegate.loadWithList(installments)
    }
    
    override func onErrorResponse(statusCode: Int?, body: String?) {
        self.delegate.loadWithError()
    }
}
