//
//  PaymentMethodGateway.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 19/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import UIKit

class PaymentMethodGateway: BaseGateway {

    private let uri = "payment_methods"
    private var delegate : PaymentMethodsInteractor?
    
    init(delegate: PaymentMethodsInteractor) {
        super.init()
        self.delegate = delegate
    }
    
    func load() {
        self.performGETRequest(uri: uri)
    }
    
    override func onResponse(response: [Any]) {
        var dictCreditCards = response as![Dictionary<String,Any>]
        dictCreditCards = dictCreditCards.filter{($0["payment_type_id"] as! String) == "credit_card"}
        let creditCards = JSONToCreditCardParser.parse(dictCreditCards)
        self.delegate?.loadWithList(creditCards)
    }
    
    override func onErrorResponse(statusCode: Int?, body: String?) {
        self.delegate?.loadWithError()
    }
    
}
