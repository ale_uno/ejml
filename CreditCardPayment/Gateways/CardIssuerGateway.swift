//
//  CardIssuerGateway.swift
//  CreditCardPayment
//
//  Created by Alejandro Uño on 21/12/2017.
//  Copyright © 2017 Alejandro Uño. All rights reserved.
//

import UIKit

class CardIssuerGateway: BaseGateway {

    private let uri = "payment_methods/card_issuers"
    private var delegate : CardIssuerInteractor
    
    init(delegate: CardIssuerInteractor) {
        self.delegate = delegate
        super.init()
    }
    
    func load(paymentMethodID : String) {
        self.performGETRequest(uri: uri, customParams: ["payment_method_id":paymentMethodID])
    }
    
    override func onResponse(response: [Any]) {
        let dictCardIssuers = response as![Dictionary<String,Any>]
        let cardIssuers = JSONToCardIssuerParser.parse(dictCardIssuers)
        self.delegate.loadWithList(cardIssuers)
    }
    
    override func onErrorResponse(statusCode: Int?, body: String?) {
        self.delegate.loadWithError()
    }
}
